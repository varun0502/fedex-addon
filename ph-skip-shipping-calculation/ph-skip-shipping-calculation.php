<?php
/*
	* Plugin Name: Skip Shipping Calculation Add-On
	* Plugin URI: https://www.xadapter.com/
	* Description: Add-on for XAdapter Woocommerce FedEx Shipping. Enhances shipping logic and calculations when combining varied shipping classes. Combines with WooCommerce Shipping Per Product module for drop shipping.
	* Author: XAdapter
	* Author URI: https://www.xadapter.com/
	* Version: 1.0.4
	* Text Domain: ph-skip-shipping-calculation
	* Copyright: 2014-2018 WooForce.
	* WC requires at least: 3.0.0
	* WC tested up to: 3.4
*/

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

register_activation_hook( __FILE__, function() {
	if( ! is_plugin_active('woocommerce/woocommerce.php') ) {
		deactivate_plugins( basename( __FILE__ ) );
			wp_die( __("Oops! You tried installing this without activating woocommerce.", 'ph-skip-shipping-calculation' ), "", array('back_link' => 1 ));
	}
});

if( ! defined('PH_SKIP_SHIPPING_CALCULATION_ID') ) {
	define( 'PH_SKIP_SHIPPING_CALCULATION_ID', 'ph_skip_shipping_calculation');
}
if( ! class_exists('Ph_Skip_Shipping_Calculation') ) {
	class Ph_Skip_Shipping_Calculation {

		public function __construct() {
			$this->id = PH_SKIP_SHIPPING_CALCULATION_ID;
			add_filter( 'woocommerce_settings_tabs_array', array( $this, 'add_settings_tab'), 50 );
			add_action( 'woocommerce_settings_tabs_'.$this->id, array( $this, 'settings_tab') );
			add_action( 'woocommerce_update_options_'.$this->id, array( $this, 'update_settings' ) );
			add_action( 'woocommerce_admin_field_add_on_rule_matrix', array( $this, 'add_on_rule_matrix' ) );
			add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ), array( $this, 'plugin_action_links' ) );


			// To remove unneccessary data
			add_filter( 'woocommerce_admin_settings_sanitize_option_ph_skip_shipping_calculation', array( $this, 'delete_unnecessary_data_before_saving'), 10, 3 );
			if( ! is_admin() ) {
				add_filter( 'wf_shipping_skip_product', array( $this, 'skip_fedex_shipping_calculation' ), 10, 3 );
			}
		}

		public function plugin_action_links( $links ) {
			$plugin_links = array(
				'<a href="' . admin_url( 'admin.php?page=wc-settings&tab=ph_skip_shipping_calculation' ) . '">' . __( 'Settings', 'ph-skip-shipping-calculation' )
			);
			return array_merge( $plugin_links, $links );
		}

		/**
		 * Decide whether product shipping calculation need to be skipped or not.
		 * @param $bool boolean True or false.
		 * @param $values array Cart line item.
		 * @param $package_contents array Cart pacakage.
		 * @bool boolean True - skip the shipping calculation or false - Don't skip the shipping calculation.
		 */
		public function skip_fedex_shipping_calculation( $bool=false, $values, $package_contents ) {
			if( $bool ) {
				return $bool;
			}

			if( empty($this->settings) ) {
				$this->settings = get_option('ph_skip_shipping_calculation');
			}

			$settings = $this->settings;
			if( $settings['status'] == 'enable' ) {
				if( ! empty($settings['rule_matrix']) ) {

					$product_shipping_class = $values['data']->get_shipping_class();
					foreach( $package_contents as $line_item ) {
						$temp_shipping_class = $line_item['data']->get_shipping_class();
						if( $product_shipping_class != $temp_shipping_class )
							$cart_shipping_classes[] = $temp_shipping_class;
					}

					foreach( $settings['rule_matrix'] as $rule ) {
						if( ! $bool && in_array( $product_shipping_class, $rule['if_this_class_exist'])) {
							if( empty($rule['shipping_classes']) ) {
								$bool = true;
							}
							else {
								$common_classes = array_intersect($rule['shipping_classes'], $cart_shipping_classes);
								if( count($common_classes) == count($rule['shipping_classes']) ) {
									$bool = true;
								}
							}
						}
					}
				}
			}
			return $bool;
		}

		public function delete_unnecessary_data_before_saving( $value, $option, $raw_value ) {
			if( is_array($value) && isset($value['rule_matrix']) ) {
				foreach( $value['rule_matrix'] as $key => $rule ) {
					if(empty(current($rule['if_this_class_exist']))) {
						unset($value['rule_matrix'][$key]);
					}
				}
			}
			return $value;
		}
		/**
		 * Add a new settings tab to the WooCommerce settings tabs array.
		 *
		 * @param array $settings_tabs Array of WooCommerce setting tabs & their labels
		 * @return array $settings_tabs Array of WooCommerce setting tabs & their labels
		 */
		public function add_settings_tab( $settings_tabs ) {
			$settings_tabs[$this->id] = __( 'Skip Shipping Calculation', 'ph-skip-shipping-calculation' );
			return $settings_tabs;
		}

		/**
		 * Uses the WooCommerce admin fields API to output settings via the @see woocommerce_admin_fields() function.
		 *
		 * @uses woocommerce_admin_fields()
		 * @uses self::get_settings()
		 */
		public function settings_tab() {
			woocommerce_admin_fields( $this->get_settings() );
		}

		/**
		 * Uses the WooCommerce options API to save settings via the @see woocommerce_update_options() function.
		 *
		 * @uses woocommerce_update_options()
		 * @uses self::get_settings()
		 */
		public function update_settings() {
			woocommerce_update_options( $this->get_settings() );
		}

		/**
		 * Get all the settings for this plugin for @see woocommerce_admin_fields() function.
		 *
		 * @return array Array of settings for @see woocommerce_admin_fields() function.
		 */
		public function get_settings() {
			$settings = array(
				'section_title' => array(
					'title'     => __( 'Section Title', 'ph-skip-shipping-calculation' ),
					'type'     => 'title',
					'desc'     => '',
					'id'       => 'ph_skip_shipping_calculation'
				),
				'status' => array(
					'title'		=> __( 'Enable / Disable', 'ph-skip-shipping-calculation' ),
					'type' 		=> 'select',
					'default'	=>	'disable',
					'desc' 		=> __( 'Enable to apply the settings', 'ph-skip-shipping-calculation' ),
					'options'	=> array(
						'enable'	=>	'Enable',
						'disable'	=>	'Disable',
					),
					'desc_tip'	=> true,
					'id'   => 'ph_skip_shipping_calculation[status]'
				),
				'add_on_rule_matrix' => array(
					'title' => __( 'Rule Matrix', 'ph-skip-shipping-calculation' ),
					'type' => 'add_on_rule_matrix',
				),
				'section_end' => array(
					'type' => 'sectionend',
					'id' => 'ph_skip_shipping_calculation'
				)
			);

			return apply_filters( 'wc_settings_ph_skip_shipping_calculation_settings', $settings );
	    }

	    public function add_on_rule_matrix() {
	    	$this->shipping_classes =WC()->shipping->get_shipping_classes();
	    	ob_start();
	    	?>
			 <table class ="wp-list-table widefat fixed posts">
					<tr>
						<th> <?php _e( "For this Shipping Class", "ph-skip-shipping-calculation"); ?> </th>
						<th> <?php _e( "With these Shipping Classes","ph-skip-shipping-calculation"); ?> </th>
					</tr>
				<?php
					$rule_count = 0;
					$settings = get_option('ph_skip_shipping_calculation');
					if( ! empty($settings['rule_matrix']) ) {
						foreach( $settings['rule_matrix'] as $rule ) {
							?>
							<tr>
								<td>
									<select name="ph_skip_shipping_calculation[rule_matrix][<?php echo $rule_count; ?>][if_this_class_exist][]">
										<?php
											echo "<option value = ''> Select Shipping Class</option>";
											foreach( $this->shipping_classes as $shipping_class ) {
												if( is_array($rule['if_this_class_exist']) && in_array( $shipping_class->slug, $rule['if_this_class_exist'] ) ) {
													echo "<option value='".$shipping_class->slug."' selected>".$shipping_class->name."</option>";
												}
												else {
													echo "<option value='".$shipping_class->slug."'>".$shipping_class->name."</option>";
												}
											}
										?>
									</select>
								</td>
								<td>
									<select class="wc-enhanced-select xa_est_shipping_class" multiple="multiple" style="width: 70%;" name="ph_skip_shipping_calculation[rule_matrix][<?php echo $rule_count; ?>][shipping_classes][]">
										<?php
											foreach( $this->shipping_classes as $shipping_class ) {
												if( isset($rule['shipping_classes']) && in_array( $shipping_class->slug, $rule['shipping_classes'] ) ) {
													echo "<option value='".$shipping_class->slug."' selected>".$shipping_class->name."</option>";
												}
												else {
													echo "<option value='".$shipping_class->slug."'>".$shipping_class->name."</option>";
												}
											}
										?>
									</select>
								</td>
							<tr/>
							<?php
							$rule_count++;
						}
					}
?>
				<tr>
					<td>
						<select name="ph_skip_shipping_calculation[rule_matrix][<?php echo $rule_count; ?>][if_this_class_exist][]">
							<?php
								echo "<option value = ''> Select Shipping Class</option>";
								foreach( $this->shipping_classes as $shipping_class ) {
									echo "<option value='".$shipping_class->slug."'>".$shipping_class->name."</option>";
								}
							?>
						</select>
					</td>
					<td>
						<select class="wc-enhanced-select xa_est_shipping_class" multiple="multiple" style="width: 70%;" name="ph_skip_shipping_calculation[rule_matrix][<?php echo $rule_count; ?>][shipping_classes][]">
							<?php
								foreach( $this->shipping_classes as $shipping_class ) {
									echo "<option value='".$shipping_class->slug."'>".$shipping_class->name."</option>";
								}
							?>
						</select>
					</td>
				</tr>
			</table>
			<?php
	    	ob_end_flush();
	    }

	}

	new Ph_Skip_Shipping_Calculation();
}