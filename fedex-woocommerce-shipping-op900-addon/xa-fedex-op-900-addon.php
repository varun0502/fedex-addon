<?php
/*
	Plugin Name: Fedex OP900 Addon
	Plugin URI: https://www.xadapter.com/product/
	Description: To print FedEx OP900 label. Recommeneded Packing method per item packing or mark product as prepacked.
	Version: 1.0.0
	Author: varun874
	Author URI: https://www.xadapter.com/vendor/wooforce/
	Copyright: XAdapter
    Text Domain: xa-fedex-op-900-addon
	WC requires at least: 3.0
	WC tested up to: 3.4
*/

// Plugin activation check
register_activation_hook( __FILE__, function() {
	//check if Premium version fedex is active or not
	if ( ! is_plugin_active('fedex-woocommerce-shipping/fedex-woocommerce-shipping.php') ) {
		deactivate_plugins( basename( __FILE__ ) );
		wp_die( __("Oops! You tried installing the FedEx OP900 addon without activating WooCommerce FedEx Shipping Plugin with Print Label. Please install and activate WooCommerce FedEx Shipping Plugin with Print Label first and then install this FedEx OP900 addon", "xa-fedex-op-900-addon" ), "", array('back_link' => 1 ));
	}
});

if (!defined('WF_Fedex_ID')){
	define("WF_Fedex_ID", "wf_fedex_woocommerce_shipping");
}

if( ! class_exists('XA_FEDEX_OP900_ADDON') ) {
	class XA_FEDEX_OP900_ADDON {

		public function __construct() {

			if( is_admin() ) {
				// Add custom fields in product shipping tab
				add_action( 'woocommerce_product_options_shipping', array( $this, 'add_custom_field_to_product_shipping_tab' ) );
				// Save custom fields of product shipping tab
				add_action( 'woocommerce_process_product_meta', array( $this, 'save_custom_product_fields_on_shipping_tab' ) );
				// Add custom fields in product variations
				// add_action( 'woocommerce_product_after_variable_attributes', array( $this, 'add_custom_product_fields_at_variation'), 10, 3 );
				// Save custom fields of product variations
				// add_action( 'woocommerce_save_product_variation', array( $this, 'save_custome_product_fields_at_variation'), 10, 2 );
			}

			add_action( 'wf_fedex_request', array( $this, 'add_hazardous_details_to_the_shipping_request' ), 10, 3 );
		}

		public function add_custom_field_to_product_shipping_tab() {

			// Id for Hazardous commodity
			woocommerce_wp_text_input(
				array(
					'id' 			=> '_xa_fedex_hz_id',
					'label' 		=> __( 'Regulatory identifier (FedEx)', 'xa-fedex-op-900-addon' ),
					'description' 	=> __( 'Id for Hazardous material. Required for OP900 Label. ', 'xa-fedex-op-900-addon' ),
					'desc_tip' 		=> 'true',
					'placeholder' 	=> 'e.g. UN1073'
				)
			);

            //ProperShippingName
			woocommerce_wp_text_input(
				array(
					'id' 			=> '_xa_fedex_proper_shipping_name',
					'label' 		=> __( 'Proper Shipping Name (FedEx)', 'xa-fedex-op-900-addon' ),
					'description' 	=> __( 'ProperShippingName for Hazardous material. Required for OP900 Label. ', 'xa-fedex-op-900-addon' ),
					'desc_tip' 		=> 'true',
					'placeholder' 	=> 'e.g. Oxygen, refrigerated liquid'
				)
			);

			//TechnicalName
			woocommerce_wp_text_input(
				array(
					'id' 			=> '_xa_fedex_technical_name',
					'label' 		=> __( 'Technical Name (FedEx)', 'xa-fedex-op-900-addon' ),
					'description' 	=> __( 'TechnicalName for Hazardous material. Required for OP900 Label. ', 'xa-fedex-op-900-addon' ),
					'desc_tip' 		=> 'true',
					'placeholder' 	=> 'e.g. Oxygen only'
				)
			);

			//HazardClass
			woocommerce_wp_text_input(
				array(
					'id' 			=> '_xa_fedex_hazard_class',
					'label' 		=> __( 'Hazard Class (FedEx)', 'xa-fedex-op-900-addon' ),
					'description' 	=> __( 'HazardClass for Hazardous material. Required for OP900 Label. ', 'xa-fedex-op-900-addon' ),
					'desc_tip' 		=> 'true',
					'placeholder' 	=> 'e.g. 2.2'
				)
			);

			//SubsidiaryClasses
			woocommerce_wp_text_input(
				array(
					'id' 			=> '_xa_fedex_subsidiary_class',
					'label' 		=> __( 'Subsidiary Class (FedEx)', 'xa-fedex-op-900-addon' ),
					'description' 	=> __( 'SubsidiaryClasses for Hazardous material. Required for OP900 Label. ', 'xa-fedex-op-900-addon' ),
					'desc_tip' 		=> 'true',
					'placeholder' 	=> 'e.g. 5.1'
				)
			);

			//SubsidiaryClasses
			woocommerce_wp_text_input(
				array(
					'id' 			=> '_xa_fedex_hz_packaging_unit',
					'label' 		=> __( 'Packaging Unit (FedEx)', 'xa-fedex-op-900-addon' ),
					'description' 	=> __( 'Package material which has been used to pack Hazardous content. Required for OP900 Label. ', 'xa-fedex-op-900-addon' ),
					'desc_tip' 		=> 'true',
					'placeholder' 	=> ' e.g. steel'
				)
			);

			// EmergencyContactNumber
			woocommerce_wp_select( array(
				'id'			=> '_xa_fedex_emergency_contact_number',
				'label'			=> __( 'Emergency Contact Number (Fedex)', 'xa-fedex-op-900-addon'),
				'description'	=> __( 'EmergencyContactNumber, Requred for OP900 Label. If nothing selected then Shipper Contact number will go', 'xa-fedex-op-900-addon'),
				'desc_tip'		=> true,
				'options'		=> array(
						null		=> __( 'Select Anyone', 'xa-fedex-op-900-addon' ),
						'SENDER'	=> __( 'Sender Contact Number', 'xa-fedex-op-900-addon' ),
						'RECEIVER'	=> __( 'Receiver Contact Number', 'xa-fedex-op-900-addon' ),
				),
		    ));

		}

		/**
		 * Save Product Custom field of Shipping Tab.
		 * @param $pos_id int Current product id
		 */
		public function save_custom_product_fields_on_shipping_tab( $post_id ) {

			// Save Id for hazardous commodity
			if( ! empty($_POST['_xa_fedex_hz_id']) ) {
				update_post_meta( $post_id, '_xa_fedex_hz_id', $_POST['_xa_fedex_hz_id'] );
			}

			// Save Proper shipping name
			if( ! empty($_POST['_xa_fedex_proper_shipping_name']) ) {
				update_post_meta( $post_id, '_xa_fedex_proper_shipping_name', $_POST['_xa_fedex_proper_shipping_name'] );
			}

			// Save Fedex Technical name
			if( ! empty($_POST['_xa_fedex_technical_name']) ) {
				update_post_meta( $post_id, '_xa_fedex_technical_name', $_POST['_xa_fedex_technical_name'] );
			}

			// Save Hazardous Class
			if( ! empty($_POST['_xa_fedex_hazard_class']) ) {
				update_post_meta( $post_id, '_xa_fedex_hazard_class', $_POST['_xa_fedex_hazard_class'] );
			}

			// Save Subsidiary Claa
			if( ! empty($_POST['_xa_fedex_subsidiary_class']) ) {
				update_post_meta( $post_id, '_xa_fedex_subsidiary_class', $_POST['_xa_fedex_subsidiary_class'] );
			}

			// Save Packaging Unit
			if( ! empty($_POST['_xa_fedex_hz_packaging_unit']) ) {
				update_post_meta( $post_id, '_xa_fedex_hz_packaging_unit', $_POST['_xa_fedex_hz_packaging_unit'] );
			}

			// Save emergency contact number
			if( ! empty($_POST['_xa_fedex_emergency_contact_number']) ) {
				update_post_meta( $post_id, '_xa_fedex_emergency_contact_number', $_POST['_xa_fedex_emergency_contact_number'] );
			}
		}

		public function add_custom_product_fields_at_variation() {

		}

		public function save_custome_product_fields_at_variation() {

		}

		/**
		 * Add Hazardous material details to the FedEx Shipping request.
		 * @param $request array Fedex Request as an array
		 * @param $order 
		 * @param $parcel
		 */
		public function add_hazardous_details_to_the_shipping_request( $request, $order, $parcel ) {

			if( $order instanceof wf_order ) {
				$order = wc_get_order($order->id);
			}
			$hazardous_product = false;
			if( ! empty( $parcel['packed_products'] ) ) {
				$products = $parcel['packed_products'];
				$processed_product = array();

				if( $parcel['service'] == 'FEDEX_GROUND' ) {

					foreach( $request['RequestedShipment']['RequestedPackageLineItems'] as &$req ) {
						
						do {
							$product = array_shift($products);

							if( $product instanceof wf_product ) {
								if( ! empty($product->obj) ) {
									$product = $product->obj;
								}
								elseif( ! empty($product->variation_id) ) {
									$product = wc_get_product($product->variation_id);
								}
								else {
									$product = wc_get_product($product->id);
								}
							}
							$product_id = $product->get_id();
						}while( in_array( $product_id, $processed_product) );

						$processed_product[] = $product_id;		// Processed Product

						if( ! empty ( $req['SpecialServicesRequested']['DangerousGoodsDetail'] ) ) {

							// Fedex Settings
							if( empty($this->fedex_settings) ) {
								$this->fedex_settings = get_option( 'woocommerce_'.WF_Fedex_ID.'_settings', array() );
							}
							$hazardous_product = true;
							$emergency_contact = $product->get_meta('_xa_fedex_emergency_contact_number'); 	// SENDER or Receiver

							if( $emergency_contact == 'RECEIVER' ) {
								$phone_number = $order->get_billing_phone();
							}
							else {
								$phone_number = $this->fedex_settings['shipper_phone_number'];
							}

							$req['SpecialServicesRequested']['DangerousGoodsDetail'] = array(
								'Options'						=>	'HAZARDOUS_MATERIALS',
								'Containers'					=>	array(
									'HazardousCommodities'	=>	array(
										'Description'	=>	array(
											'Id'					=>	$product->get_meta('_xa_fedex_hz_id'),
											'ProperShippingName'	=>	$product->get_meta('_xa_fedex_proper_shipping_name'),
											'TechnicalName'			=>	$product->get_meta('_xa_fedex_technical_name'),
											'HazardClass'			=>	$product->get_meta('_xa_fedex_hazard_class'),
											'SubsidiaryClasses'		=>	$product->get_meta('_xa_fedex_subsidiary_class'),
											'LabelText'				=>	'HZ',
										),
									),
								),
								'EmergencyContactNumber'		=>	$phone_number,
								'Offeror'						=>	$this->fedex_settings['shipper_person_name'],
								'Packaging'						=>	array(
									'Count'		=>	1,
									'Units'		=>	$product->get_meta('_xa_fedex_hz_packaging_unit'),
								),
							);
						}
					}
				}
			}

			// If Hazardeous material found
			if( $hazardous_product ) {
				$request['RequestedShipment']['ShippingDocumentSpecification'] = array(
					'ShippingDocumentTypes'	=>	'OP_900',
					'Op900Detail'			=> 		array(
						'Format'		=>	array(
							'ImageType'	=>	'PDF',
							'StockType'	=>	'OP_900_LL_B',
						),
						'SignatureName'		=>	$this->fedex_settings['shipper_person_name'],
					),
				);
			}

			return $request;
		}

	}

	new XA_FEDEX_OP900_ADDON();
}